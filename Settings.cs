using GlobalHotKey;
using System.Configuration;
using System.Windows.Forms;
using System.Windows.Input;

namespace VirtualDesktopManager
{

    static class Settings
    {
        public static ModifierKeys SwitchPrefix;
        public static ModifierKeys MovePrefix;
        public static Key PinShortcut;
        public static Key LeftShortcut;
        public static Key RightShortcut;
        public static Key Desktop1Shortcut;
        public static Key Desktop2Shortcut;
        public static Key Desktop3Shortcut;
        public static Key Desktop4Shortcut;
        public static Key Desktop5Shortcut;
        public static Key Desktop6Shortcut;
        public static Key Desktop7Shortcut;
        public static Key Desktop8Shortcut;
        public static Key Desktop9Shortcut;
        public static Key Desktop10Shortcut;

        private static ModifierKeys ParseModifiers(string input)
        {
            ModifierKeysConverter mkc = new ModifierKeysConverter();
            ModifierKeys modifiers = (ModifierKeys)mkc.ConvertFromString(input);

            return modifiers;
        }

        private static Key ParseKey(string input)
        {
            KeyConverter kc = new KeyConverter();

            if (input.Split("+").Length > 1)
            {
                MessageBox.Show("Cannot accept multiple non-modifer hotkeys per action.", "Error: Invalid Configuration", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            var key = (Key)kc.ConvertFromString(input);

            return key;
        }

        public static void UpdateSettings()
        {
            SwitchPrefix = ParseModifiers(ConfigurationManager.AppSettings["SwitchPrefix"]);
            MovePrefix = ParseModifiers(ConfigurationManager.AppSettings["MovePrefix"]);

            PinShortcut = ParseKey(ConfigurationManager.AppSettings["PinShortcut"]);
            LeftShortcut = ParseKey(ConfigurationManager.AppSettings["LeftShortcut"]);
            RightShortcut = ParseKey(ConfigurationManager.AppSettings["RightShortcut"]);
            Desktop1Shortcut = ParseKey(ConfigurationManager.AppSettings["Desktop1Shortcut"]);
            Desktop2Shortcut = ParseKey(ConfigurationManager.AppSettings["Desktop2Shortcut"]);
            Desktop3Shortcut = ParseKey(ConfigurationManager.AppSettings["Desktop3Shortcut"]);
            Desktop4Shortcut = ParseKey(ConfigurationManager.AppSettings["Desktop4Shortcut"]);
            Desktop5Shortcut = ParseKey(ConfigurationManager.AppSettings["Desktop5Shortcut"]);
            Desktop6Shortcut = ParseKey(ConfigurationManager.AppSettings["Desktop6Shortcut"]);
            Desktop7Shortcut = ParseKey(ConfigurationManager.AppSettings["Desktop7Shortcut"]);
            Desktop8Shortcut = ParseKey(ConfigurationManager.AppSettings["Desktop8Shortcut"]);
            Desktop9Shortcut = ParseKey(ConfigurationManager.AppSettings["Desktop9Shortcut"]);
            Desktop10Shortcut = ParseKey(ConfigurationManager.AppSettings["Desktop10Shortcut"]);
        }
    }
}