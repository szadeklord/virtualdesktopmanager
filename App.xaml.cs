﻿using GlobalHotKey;
using Notification.Wpf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Forms;
using WindowsDesktop;

namespace VirtualDesktopManager
{

    public partial class App : System.Windows.Application
    {
        private readonly HotKeyManager _hotKeyManager = new HotKeyManager();
        private List<HotKey> _activeHotkeys = new List<HotKey>();
        private System.Windows.Forms.NotifyIcon _notifyIcon;
        private bool _isExit;
        private NotificationManager _notificationManager = new NotificationManager();

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            MainWindow = new MainWindow();
            MainWindow.Closing += MainWindow_Closing;

            _notifyIcon = new System.Windows.Forms.NotifyIcon();
            _notifyIcon.DoubleClick += (s, args) => UpdateFromSettings();
            _notifyIcon.Icon = Icon.ExtractAssociatedIcon(Assembly.GetExecutingAssembly().Location);
            _notifyIcon.Visible = true;

            UpdateFromSettings();

            CreateContextMenu();

            // Add hotkey callbacks below
            // Switch Shortcuts
            // We dont add switch shortcuts for left and right as they are already supported by the OS
            _activeHotkeys.Add(_hotKeyManager.Register(Settings.Desktop1Shortcut, Settings.SwitchPrefix));
            _activeHotkeys.Add(_hotKeyManager.Register(Settings.Desktop2Shortcut, Settings.SwitchPrefix));
            _activeHotkeys.Add(_hotKeyManager.Register(Settings.Desktop3Shortcut, Settings.SwitchPrefix));
            _activeHotkeys.Add(_hotKeyManager.Register(Settings.Desktop4Shortcut, Settings.SwitchPrefix));
            _activeHotkeys.Add(_hotKeyManager.Register(Settings.Desktop5Shortcut, Settings.SwitchPrefix));
            _activeHotkeys.Add(_hotKeyManager.Register(Settings.Desktop6Shortcut, Settings.SwitchPrefix));
            _activeHotkeys.Add(_hotKeyManager.Register(Settings.Desktop7Shortcut, Settings.SwitchPrefix));
            _activeHotkeys.Add(_hotKeyManager.Register(Settings.Desktop8Shortcut, Settings.SwitchPrefix));
            _activeHotkeys.Add(_hotKeyManager.Register(Settings.Desktop9Shortcut, Settings.SwitchPrefix));
            _activeHotkeys.Add(_hotKeyManager.Register(Settings.Desktop10Shortcut, Settings.SwitchPrefix));

            // Move Shortcuts
            _activeHotkeys.Add(_hotKeyManager.Register(Settings.LeftShortcut, Settings.MovePrefix));
            _activeHotkeys.Add(_hotKeyManager.Register(Settings.RightShortcut, Settings.MovePrefix));
            _activeHotkeys.Add(_hotKeyManager.Register(Settings.Desktop1Shortcut, Settings.MovePrefix));
            _activeHotkeys.Add(_hotKeyManager.Register(Settings.Desktop2Shortcut, Settings.MovePrefix));
            _activeHotkeys.Add(_hotKeyManager.Register(Settings.Desktop3Shortcut, Settings.MovePrefix));
            _activeHotkeys.Add(_hotKeyManager.Register(Settings.Desktop4Shortcut, Settings.MovePrefix));
            _activeHotkeys.Add(_hotKeyManager.Register(Settings.Desktop5Shortcut, Settings.MovePrefix));
            _activeHotkeys.Add(_hotKeyManager.Register(Settings.Desktop6Shortcut, Settings.MovePrefix));
            _activeHotkeys.Add(_hotKeyManager.Register(Settings.Desktop7Shortcut, Settings.MovePrefix));
            _activeHotkeys.Add(_hotKeyManager.Register(Settings.Desktop8Shortcut, Settings.MovePrefix));
            _activeHotkeys.Add(_hotKeyManager.Register(Settings.Desktop9Shortcut, Settings.MovePrefix));
            _activeHotkeys.Add(_hotKeyManager.Register(Settings.Desktop10Shortcut, Settings.MovePrefix));

            _hotKeyManager.KeyPressed += HotKeyManagerPressed;

            VirtualDesktop.Configure();
            VirtualDesktop.CurrentChanged += DesktopChangedHandler;
        }

        private void DesktopChangedHandler(object sender, VirtualDesktopChangedEventArgs args)
        {
            int newIndex = WindowHelper.GetDesktopIndex(args.NewDesktop);
            var timespan = new TimeSpan(0, 0, 0, 0, 750);

            _notificationManager.Show("Virtual Desktop: " + newIndex, "", expirationTime: timespan, ShowXbtn: false);
        }

        private void CreateContextMenu()
        {
            _notifyIcon.ContextMenuStrip = new System.Windows.Forms.ContextMenuStrip();
            _notifyIcon.ContextMenuStrip.Items.Add("Reload app.config").Click += (s, e) => UpdateFromSettings();
            _notifyIcon.ContextMenuStrip.Items.Add("Exit").Click += (s, e) => ExitApplication();

            // The following steps are neccesary so that the user is able to utilize the control as an intermediary to invoke events on the thread pool.
            // For whatever reason the VirtualDesktop library will not work on the callback thread and will only function if invoked on the Gui thread. 
            // Additionally the handle for the context is not yet created by default until the user interacts with the tray icon. We force its creation by accessing the C# property and assign it to a temp variable.
            if (!_notifyIcon.ContextMenuStrip.IsHandleCreated)
            {
                _notifyIcon.ContextMenuStrip.CreateControl();
            }

            if (!_notifyIcon.ContextMenuStrip.IsHandleCreated)
            {
                var temp = _notifyIcon.ContextMenuStrip.Handle;
            }
        }

        private delegate void VirtualDesktopDelegate();
        private delegate void VirtualDesktopIndexDelegate(int desktopNumber);

        private void HotKeyManagerPressed(object sender, KeyPressedEventArgs e)
        {
            // Switch Desktop Handling
            if (new HotKey(Settings.Desktop1Shortcut, Settings.SwitchPrefix).Equals(e.HotKey))
            {
                _notifyIcon.ContextMenuStrip.BeginInvoke((VirtualDesktopIndexDelegate)WindowHelper.SwitchToDesktopIfExists, 0);
            }
            else if (new HotKey(Settings.Desktop2Shortcut, Settings.SwitchPrefix).Equals(e.HotKey))
            {
                _notifyIcon.ContextMenuStrip.BeginInvoke((VirtualDesktopIndexDelegate)WindowHelper.SwitchToDesktopIfExists, 1);
            }
            else if (new HotKey(Settings.Desktop3Shortcut, Settings.SwitchPrefix).Equals(e.HotKey))
            {
                _notifyIcon.ContextMenuStrip.BeginInvoke((VirtualDesktopIndexDelegate)WindowHelper.SwitchToDesktopIfExists, 2);
            }
            else if (new HotKey(Settings.Desktop4Shortcut, Settings.SwitchPrefix).Equals(e.HotKey))
            {
                _notifyIcon.ContextMenuStrip.BeginInvoke((VirtualDesktopIndexDelegate)WindowHelper.SwitchToDesktopIfExists, 3);
            }
            else if (new HotKey(Settings.Desktop5Shortcut, Settings.SwitchPrefix).Equals(e.HotKey))
            {
                _notifyIcon.ContextMenuStrip.BeginInvoke((VirtualDesktopIndexDelegate)WindowHelper.SwitchToDesktopIfExists, 4);
            }
            else if (new HotKey(Settings.Desktop6Shortcut, Settings.SwitchPrefix).Equals(e.HotKey))
            {
                _notifyIcon.ContextMenuStrip.BeginInvoke((VirtualDesktopIndexDelegate)WindowHelper.SwitchToDesktopIfExists, 5);
            }
            else if (new HotKey(Settings.Desktop7Shortcut, Settings.SwitchPrefix).Equals(e.HotKey))
            {
                _notifyIcon.ContextMenuStrip.BeginInvoke((VirtualDesktopIndexDelegate)WindowHelper.SwitchToDesktopIfExists, 6);
            }
            else if (new HotKey(Settings.Desktop8Shortcut, Settings.SwitchPrefix).Equals(e.HotKey))
            {
                _notifyIcon.ContextMenuStrip.BeginInvoke((VirtualDesktopIndexDelegate)WindowHelper.SwitchToDesktopIfExists, 7);
            }
            else if (new HotKey(Settings.Desktop9Shortcut, Settings.SwitchPrefix).Equals(e.HotKey))
            {
                _notifyIcon.ContextMenuStrip.BeginInvoke((VirtualDesktopIndexDelegate)WindowHelper.SwitchToDesktopIfExists, 8);
            }
            else if (new HotKey(Settings.Desktop10Shortcut, Settings.SwitchPrefix).Equals(e.HotKey))
            {
                _notifyIcon.ContextMenuStrip.BeginInvoke((VirtualDesktopIndexDelegate)WindowHelper.SwitchToDesktopIfExists, 9);
            } // Move-To Desktop Handling 
            else if (new HotKey(Settings.Desktop1Shortcut, Settings.MovePrefix).Equals(e.HotKey))
            {
                _notifyIcon.ContextMenuStrip.BeginInvoke((VirtualDesktopIndexDelegate)WindowHelper.MoveToDesktopIfExists, 0);
            }
            else if (new HotKey(Settings.Desktop2Shortcut, Settings.MovePrefix).Equals(e.HotKey))
            {
                _notifyIcon.ContextMenuStrip.BeginInvoke((VirtualDesktopIndexDelegate)WindowHelper.MoveToDesktopIfExists, 1);
            }
            else if (new HotKey(Settings.Desktop3Shortcut, Settings.MovePrefix).Equals(e.HotKey))
            {
                _notifyIcon.ContextMenuStrip.BeginInvoke((VirtualDesktopIndexDelegate)WindowHelper.MoveToDesktopIfExists, 2);
            }
            else if (new HotKey(Settings.Desktop4Shortcut, Settings.MovePrefix).Equals(e.HotKey))
            {
                _notifyIcon.ContextMenuStrip.BeginInvoke((VirtualDesktopIndexDelegate)WindowHelper.MoveToDesktopIfExists, 3);
            }
            else if (new HotKey(Settings.Desktop5Shortcut, Settings.MovePrefix).Equals(e.HotKey))
            {
                _notifyIcon.ContextMenuStrip.BeginInvoke((VirtualDesktopIndexDelegate)WindowHelper.MoveToDesktopIfExists, 4);
            }
            else if (new HotKey(Settings.Desktop6Shortcut, Settings.MovePrefix).Equals(e.HotKey))
            {
                _notifyIcon.ContextMenuStrip.BeginInvoke((VirtualDesktopIndexDelegate)WindowHelper.MoveToDesktopIfExists, 5);
            }
            else if (new HotKey(Settings.Desktop7Shortcut, Settings.MovePrefix).Equals(e.HotKey))
            {
                _notifyIcon.ContextMenuStrip.BeginInvoke((VirtualDesktopIndexDelegate)WindowHelper.MoveToDesktopIfExists, 6);
            }
            else if (new HotKey(Settings.Desktop8Shortcut, Settings.MovePrefix).Equals(e.HotKey))
            {
                _notifyIcon.ContextMenuStrip.BeginInvoke((VirtualDesktopIndexDelegate)WindowHelper.MoveToDesktopIfExists, 7);
            }
            else if (new HotKey(Settings.Desktop9Shortcut, Settings.MovePrefix).Equals(e.HotKey))
            {
                _notifyIcon.ContextMenuStrip.BeginInvoke((VirtualDesktopIndexDelegate)WindowHelper.MoveToDesktopIfExists, 8);
            }
            else if (new HotKey(Settings.Desktop10Shortcut, Settings.MovePrefix).Equals(e.HotKey))
            {
                _notifyIcon.ContextMenuStrip.BeginInvoke((VirtualDesktopIndexDelegate)WindowHelper.MoveToDesktopIfExists, 9);
            }
            else if (new HotKey(Settings.LeftShortcut, Settings.MovePrefix).Equals(e.HotKey))
            {
                _notifyIcon.ContextMenuStrip.BeginInvoke((VirtualDesktopDelegate)WindowHelper.MoveLeftIfExists);
            }
            else if (new HotKey(Settings.RightShortcut, Settings.MovePrefix).Equals(e.HotKey))
            {
                _notifyIcon.ContextMenuStrip.BeginInvoke((VirtualDesktopDelegate)WindowHelper.MoveRightIfExists);
            }
        }

        private void ExitApplication()
        {
            _isExit = true;
            MainWindow.Close();
            _notifyIcon.Dispose();
            _notifyIcon = null;
            _hotKeyManager.Dispose();
        }

        private void UpdateFromSettings()
        {
            try
            {
                Settings.UpdateSettings();
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, "Error updating from app.config");
            }
        }

        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (!_isExit)
            {
                e.Cancel = true;
                MainWindow.Hide(); // A hidden window can be shown again, a closed one can not
            }
        }
    }
}